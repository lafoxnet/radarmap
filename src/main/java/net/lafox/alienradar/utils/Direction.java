package net.lafox.alienradar.utils;

/**
 * Created by Oleg Tsymaenko on 11/14/17.
 */
public enum Direction {
    NORTH(0, -1),
    EAST(1, 0),
    SOUTH(0, 1),
    WEST(-1, 0),
    NORTH_EAST(EAST.deltaX, NORTH.deltaY),
    NORTH_WEST(WEST.deltaX, NORTH.deltaY),
    SOUTH_EAST(EAST.deltaX, SOUTH.deltaY),
    SOUTH_WEST(WEST.deltaX, SOUTH.deltaY);

    private final int deltaX;
    private final int deltaY;

    Direction(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    public int getDeltaX() {
        return deltaX;
    }

    public int getDeltaY() {
        return deltaY;
    }
}
