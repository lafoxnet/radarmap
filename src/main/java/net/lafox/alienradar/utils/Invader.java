package net.lafox.alienradar.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

import static net.lafox.alienradar.utils.InitialData.INVADERS_MAP;


public class Invader extends Matrix {
    @Getter
    @Setter
    private String id = UUID.randomUUID().toString();

    @Getter
    private String name;

    @Getter
    private int topLeftX;

    @Getter
    private int topLeftY;

    @Getter
    @Setter
    private Direction prevDirection;

    public Invader(Invader invader, int topLeftX, int topLeftY) {
        super(invader);
        this.topLeftX = topLeftX;
        this.topLeftY = topLeftY;
        this.name = invader.name;
    }

    public Invader(String invaderName) {
        super(INVADERS_MAP.get(invaderName));
        this.topLeftX = 0;
        this.topLeftY = 0;
        this.name = invaderName;
    }

    public Invader cloneMoved(Direction direction) {
        Invader invader = new Invader(this, topLeftX + direction.getDeltaX(), topLeftY + direction.getDeltaY());
        invader.id = id;
        return invader;
    }

    public void move(Direction direction) {
        topLeftX += direction.getDeltaX();
        topLeftY += direction.getDeltaY();
        prevDirection = direction;
    }

    public void move(int x, int y) {
        topLeftX = x;
        topLeftY = y;
        prevDirection = null;
    }

    private boolean valueInRange(int value, int min, int max) {
        return value >= min && value <= max;
    }

    private boolean rectOverlap(Invader invader) {
        boolean xOverlap = valueInRange(this.topLeftX, invader.topLeftX, invader.topLeftX + invader.getWidth()) ||
                valueInRange(invader.topLeftX, this.topLeftX, this.topLeftX + this.getWidth());

        boolean yOverlap = valueInRange(this.topLeftY, invader.topLeftY, invader.topLeftY + invader.getHeight()) ||
                valueInRange(invader.topLeftY, this.topLeftY, this.topLeftY + this.getHeight());

        return xOverlap && yOverlap;
    }

    public boolean isOverlap(Invader invader) {
        return rectOverlap(invader);
    }


}
