package net.lafox.alienradar.utils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

@SuppressWarnings("WeakerAccess")
public class InitialData {
    public static final char EMPTY_POINT = '-';
    public static final char INVADER_POINT = 'o';

    public static final String INVADER_1 = "" +
            "--o-----o--\n" +
            "---o---o---\n" +
            "--ooooooo--\n" +
            "-oo-ooo-oo-\n" +
            "ooooooooooo\n" +
            "o-ooooooo-o\n" +
            "o-o-----o-o\n" +
            "---oo-oo---\n";

    public static final String INVADER_2 = "" +
            "---oo---\n" +
            "--oooo--\n" +
            "-oooooo-\n" +
            "oo-oo-oo\n" +
            "oooooooo\n" +
            "--o--o--\n" +
            "-o-oo-o-\n" +
            "o-o--o-o\n";

    public static final String INVADER_3 = "" +
            "-------o-------\n" +
            "-----ooooo-----\n" +
            "---oo--o--oo---\n" +
            "ooooooooooooooo\n" +
            "---oo-----oo---\n" +
            "--o--ooooo--o--\n" +
            "-oo---------oo-\n";

    public static final Map<String, String> INVADERS_MAP = ImmutableMap.<String, String>builder()
            .put("INVADER_1", INVADER_1)
            .put("INVADER_2", INVADER_2)
            .put("INVADER_3", INVADER_3)
            .build();
    public static final List<String> INVADER_NAMES = ImmutableList.copyOf(INVADERS_MAP.keySet());
}
