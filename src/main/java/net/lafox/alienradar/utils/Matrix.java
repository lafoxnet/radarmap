package net.lafox.alienradar.utils;

/**
 * Created by Oleg Tsymaenko on 11/14/17.
 */
public class Matrix {
    private final char[][] data;
    private final int width;
    private final int height;

    public Matrix(int width, int height) {
        data = new char[height][width];
        this.height = height;
        this.width = width;
    }

    public Matrix(Matrix matrix) {
        this.height = matrix.height;
        this.width = matrix.width;
        this.data = new char[matrix.getData().length][];

        for (int i = 0; i < matrix.getData().length; i++) {
            this.data[i] = matrix.getData()[i].clone();
        }
    }

    public Matrix(String spriteStr) {
        String[] split = spriteStr.trim().split("\n");
        this.height = split.length;
        this.width = split[0].length();
        this.data = new char[height][width];
        for (int y = 0; y < height; y++) {
            this.data[y] = split[y].toCharArray();
        }
    }

    public Matrix getSubMatrix(int topLeftX, int topLeftY, int width, int height) {
        Matrix result = new Matrix(width, height);
        int maxX = Math.min(topLeftX + result.width, this.width);
        int maxY = Math.min(topLeftY + result.height, this.height);
        int minX = Math.max(topLeftX, 0);
        int minY = Math.max(topLeftY, 0);
        for (int x = minX; x < maxX; x++) {
            for (int y = minY; y < maxY; y++) {
                result.setXY(x - topLeftX, y - topLeftY, this.getXY(x, y));
            }
        }
        return result;
    }

    public void copyDataFromMatrix(int topLeftX, int topLeftY, Matrix matrix) {
        int maxX = Math.min(topLeftX + matrix.width, this.width);
        int maxY = Math.min(topLeftY + matrix.height, this.height);
        int minX = Math.max(topLeftX, 0);
        int minY = Math.max(topLeftY, 0);
        for (int x = minX; x < maxX; x++) {
            for (int y = minY; y < maxY; y++) {
                this.setXY(x, y, matrix.getXY(x - topLeftX, y - topLeftY));
            }
        }
    }

    public void cleanup() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                this.setXY(x, y, InitialData.EMPTY_POINT);
            }
        }
    }

    public void setXY(int x, int y, char ch) {
        data[y][x] = ch;
    }

    public char getXY(int x, int y) {
        return data[y][x];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char[][] getData() {
        return data;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (int row = 0; row < height; row++) {
            res.append(data[row]).append('\n');
        }
        return res.toString();
    }


}
