package net.lafox.alienradar.utils;

import lombok.Getter;
import net.lafox.alienradar.dto.RadarMapDto;

import java.util.*;

/**
 * Created by Oleg Tsymaenko on 11/14/17.
 */
public class RadarMap extends Matrix {

    private static final Random RANDOM = new Random();

    @Getter
    private final List<Invader> invaders = new ArrayList<>();

    @Getter
    private RadarMapDto radarMapDto;

    public RadarMap(int width, int height) {
        super(width, height);
    }

    public void addInvader(Invader invader) {
        invaders.add(invader);
    }

    public boolean isOverlap(Invader invader) {
        for (Invader i : invaders) {
            if (i.isOverlap(invader)) return true;
        }
        return false;
    }


    public void moveAllInvadersInRandomDirection() {
        for (Invader invander : invaders) {
            Set<Direction> triedDirections = new HashSet<>();

            while (triedDirections.size() < Direction.values().length) {
                Direction direction = invander.getPrevDirection() != null ? invander.getPrevDirection() : randomEnum(Direction.class);
                if (!isOverlapped(invander, direction, triedDirections) && !isOutFromMap(invander, direction, triedDirections)) {
                    invander.move(direction);
                    break;
                }
                invander.setPrevDirection(null);
            }
        }
    }

    public void draw(int positiveNoisePercent, int negativeNoisePercent) {
        cleanup();
        for (Invader invader : invaders) {
            copyDataFromMatrix(invader.getTopLeftX(), invader.getTopLeftY(), invader);
        }
        addPositiveNoise(positiveNoisePercent);
        addNegativeNoise(negativeNoisePercent);

        radarMapDto = RadarMapDto.builder()
                .id(UUID.randomUUID().toString())
                .date(new Date())
                .width(getWidth())
                .height(getHeight())
                .map(toString())
                .build();

    }

    private boolean isOverlapped(Invader currentInvander, Direction direction, Set<Direction> triedDirections) {
        Invader invaderMovedInRandomDirection = currentInvander.cloneMoved(direction);

        boolean isOverlapped = false;
        for (Invader invader : invaders) {
            if (invader.getId().equals(currentInvander.getId())) continue;
            if (invader.isOverlap(invaderMovedInRandomDirection)) {
                isOverlapped = true;
                triedDirections.add(direction);
                break;
            }
        }
        return isOverlapped;
    }

    private boolean isOutFromMap(Invader currentInvander, Direction direction, Set<Direction> triedDirections) {
        Invader invaderMovedInRandomDirection = currentInvander.cloneMoved(direction);

        if (invaderMovedInRandomDirection.getTopLeftX() < 0
                || invaderMovedInRandomDirection.getTopLeftY() < 0
                || invaderMovedInRandomDirection.getTopLeftX() > getWidth() - invaderMovedInRandomDirection.getWidth()
                || invaderMovedInRandomDirection.getTopLeftY() > getHeight() - invaderMovedInRandomDirection.getHeight()
                ) {
            triedDirections.add(direction);
            return true;
        }
        return false;
    }

    private <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        int x = RANDOM.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    private void distortion(int percent, char ch) {
        for (int n = 0; n < getWidth() * getHeight() * percent / 100; n++) {
            int x = RANDOM.nextInt(getWidth());
            int y = RANDOM.nextInt(getHeight());
            setXY(x, y, ch);
        }
    }

    private void addPositiveNoise(int percent) {
        distortion(percent, InitialData.INVADER_POINT);
    }

    private void addNegativeNoise(int percent) {
        distortion(percent, InitialData.EMPTY_POINT);
    }


}
