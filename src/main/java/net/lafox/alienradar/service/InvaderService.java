package net.lafox.alienradar.service;

import net.lafox.alienradar.dto.CheckJson;
import net.lafox.alienradar.dto.CheckResponseJson;
import net.lafox.alienradar.dto.RadarMapDto;

import java.util.Map;

/**
 * Created by Oleg Tsymaenko on 11/14/17.
 */
public interface InvaderService {

    RadarMapDto generateRadarMapDto(boolean skipLogging);

    void redrawMap();

    Map<String, String> getInvaders();

    CheckResponseJson checkMyResult(CheckJson checkJson);
}
