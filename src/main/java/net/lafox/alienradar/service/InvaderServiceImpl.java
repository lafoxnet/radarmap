package net.lafox.alienradar.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import net.lafox.alienradar.dto.CheckJson;
import net.lafox.alienradar.dto.CheckResponseJson;
import net.lafox.alienradar.dto.InvaderJson;
import net.lafox.alienradar.dto.RadarMapDto;
import net.lafox.alienradar.model.RadarEntity;
import net.lafox.alienradar.repository.RadarRepository;
import net.lafox.alienradar.utils.Invader;
import net.lafox.alienradar.utils.RadarMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

import static net.lafox.alienradar.utils.InitialData.INVADERS_MAP;
import static net.lafox.alienradar.utils.InitialData.INVADER_NAMES;

/**
 * Created by Oleg Tsymaenko on 11/14/17.
 */
@Service
public class InvaderServiceImpl implements InvaderService {
    private RadarMap radarMap;
    private static final Random RANDOM = new Random();
    private static final ObjectMapper mapper = new ObjectMapper();

    @Value("${map.width:200}")
    private int mapWidth;

    @Value("${map.height:150}")
    private int mapHeight;

    @Value("${invader.count:15}")
    private int invaderCount;

    @Value("${positive.noise.percent:10}")
    private int positiveNoisePercent;

    @Value("${negative.noise.percent:10}")
    private int negativeNoisePercent;

    @Autowired
    RadarRepository radarRepository;


    @PostConstruct
    private void init() {
        radarMap = new RadarMap(mapWidth, mapHeight);
        for (int i = 0; i < invaderCount; i++) {
            Invader invader = tryInsertNewInvader();
            if (invader != null) {
                radarMap.addInvader(invader);
            }
        }
        radarMap.draw(positiveNoisePercent, negativeNoisePercent);
    }

    private Invader tryInsertNewInvader() {
        Invader invader;
        int n = 0;
        do {
            invader = getRandomInvader();
            n++;
        } while (radarMap.isOverlap(invader));
        return n > 100 ? null : invader;
    }

    private Invader getRandomInvader() {
        String invaderName = INVADER_NAMES.get(RANDOM.nextInt(INVADER_NAMES.size()));
        Invader invader = new Invader(invaderName);
        int x = RANDOM.nextInt(radarMap.getWidth() - invader.getWidth());
        int y = RANDOM.nextInt(radarMap.getHeight() - invader.getHeight());
        invader.move(x, y);

        return invader;
    }

    private void saveToDb(RadarMapDto dto) {
        if (radarRepository.findOne(dto.getId()) != null) return;
        try {
            RadarEntity entity = new RadarEntity();
            entity.setId(dto.getId());
            entity.setMap(dto.getMap());
            entity.setInvadersJson(mapper.writeValueAsString(convertInvaderToJson()));
            entity.setDate(dto.getDate());
            radarRepository.save(entity);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("JSON Creation Error", e);
        } catch (Exception e) {
            throw new RuntimeException("DB Error", e);
        }
    }

    private List<InvaderJson> convertInvaderToJson() {
        List<InvaderJson> jsonList = new ArrayList<>();
        for (Invader invader : radarMap.getInvaders()) {
            jsonList.add(InvaderJson.builder()
                    .name(invader.getName())
                    .x(invader.getTopLeftX())
                    .y(invader.getTopLeftY())
                    .build()
            );
        }
        return jsonList;
    }

    @Override
    public RadarMapDto generateRadarMapDto(boolean skipLogging) {
        RadarMapDto dto = radarMap.getRadarMapDto();
        if (!skipLogging) {
            saveToDb(dto);
        }
        return dto;
    }

    @Override
    public void redrawMap() {
        radarMap.moveAllInvadersInRandomDirection();
        radarMap.draw(positiveNoisePercent, negativeNoisePercent);
    }

    @Override
    public Map<String, String> getInvaders() {
        return INVADERS_MAP;
    }


    @Override
    public CheckResponseJson checkMyResult(CheckJson checkJson) {
        if (checkJson == null) throw new RuntimeException("Invalid JSON");
        Set<InvaderJson> actualSet = retrieveActualJson(checkJson.getMapId());
        Set<InvaderJson> used = new HashSet<>();
        for (InvaderJson real : checkJson.getDetectedInvaders()) {
            if (actualSet.contains(real)) {
                used.add(real);
            }
        }
        Set<InvaderJson> errors = new HashSet<>(checkJson.getDetectedInvaders());
        errors.removeAll(actualSet);
        return CheckResponseJson.builder()
                .mapId(checkJson.getMapId())
                .matchedCount(used.size())
                .receivedCount(checkJson.getDetectedInvaders().size())
                .percentOfSuccessDetection(used.size() * 100 / actualSet.size() - errors.size() * 100 / actualSet.size())
                .build();

    }

    private Set<InvaderJson> retrieveActualJson(String mapId) {
        if (Strings.isNullOrEmpty(mapId)) throw new RuntimeException("Map ID is empty");
        if (mapId.length() != 36) throw new RuntimeException("Bad Map ID");
        try {

            RadarEntity entity = radarRepository.findOne(mapId);
            if (entity == null) {
                throw new RuntimeException("MAP ID:" + mapId + " not found");
            }
            InvaderJson[] actualArray = mapper.readValue(entity.getInvadersJson(), InvaderJson[].class);
            return new HashSet<>(Arrays.asList(actualArray));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
