package net.lafox.alienradar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by Oleg Tsymaenko on 11/15/17.
 */
@Service
public class CronServiceImpl implements CronService {
    @Autowired
    InvaderService invaderService;

    @Scheduled(cron = "* * * * * *") //https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html
    @Override
    public void cronJob() {
        invaderService.redrawMap();
    }

}
