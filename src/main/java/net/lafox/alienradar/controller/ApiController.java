package net.lafox.alienradar.controller;

import net.lafox.alienradar.dto.CheckJson;
import net.lafox.alienradar.service.InvaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Oleg Tsymaenko on 11/15/17.
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class ApiController {

    @Autowired
    private InvaderService invaderService;

    @GetMapping(value = "/getInvaders")
    public ResponseEntity getInvaders() {
        return ResponseEntity.ok(invaderService.getInvaders());
    }

    @GetMapping(value = "/getMap")
    public ResponseEntity getMap(@RequestParam(value = "skipLogging", defaultValue = "false") boolean skipLogging) {
        return ResponseEntity.ok(invaderService.generateRadarMapDto(skipLogging));
    }

    @PostMapping(value = "/checkMyResult", consumes = "application/json", headers = "content-type=application/x-www-form-urlencoded")
    public ResponseEntity checkMyResult(@RequestBody CheckJson checkJson) {
        return ResponseEntity.ok(invaderService.checkMyResult(checkJson));
    }

}
