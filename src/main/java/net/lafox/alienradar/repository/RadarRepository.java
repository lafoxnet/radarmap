package net.lafox.alienradar.repository;

import net.lafox.alienradar.model.RadarEntity;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Oleg Tsymaenko on 11/16/17.
 */

@Transactional
public interface RadarRepository extends CrudRepository<RadarEntity, String> {

}
