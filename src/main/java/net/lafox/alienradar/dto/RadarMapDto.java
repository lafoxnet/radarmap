package net.lafox.alienradar.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Oleg Tsymaenko on 11/15/17.
 */
@Getter
@Setter
@Builder
public class RadarMapDto implements Serializable {
    private String id;
    private String map;
    private Date date;
    private int height;
    private int width;

}
