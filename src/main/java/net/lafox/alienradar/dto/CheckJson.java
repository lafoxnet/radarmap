package net.lafox.alienradar.dto;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Oleg Tsymaenko on 11/16/17.
 */
@Getter
public class CheckJson implements Serializable {
    private String mapId;
    private List<InvaderJson> detectedInvaders;
}
