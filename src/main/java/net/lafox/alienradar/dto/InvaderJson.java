package net.lafox.alienradar.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

/**
 * Created by Oleg Tsymaenko on 11/16/17.
 */
@Getter
@Builder
@EqualsAndHashCode
public class InvaderJson implements Serializable {
    private String name;

    private int x;

    private int y;
}
