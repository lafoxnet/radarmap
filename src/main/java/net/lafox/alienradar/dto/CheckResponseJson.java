package net.lafox.alienradar.dto;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * Created by Oleg Tsymaenko on 11/16/17.
 */
@Getter
@Builder
public class CheckResponseJson implements Serializable {
    private String mapId;
    private int receivedCount;
    private int matchedCount;
    private int percentOfSuccessDetection;
}
