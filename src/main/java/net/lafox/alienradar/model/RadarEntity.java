package net.lafox.alienradar.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Oleg Tsymaenko on 11/16/17.
 */
@Entity
@Table(name = "radar")

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class RadarEntity implements Serializable {

    @Id
    @Column(length = 36)
    private String id;

    private Date date;

    @Column(length = 1024 * 512)
    private String map;

    @Column(length = 1024 * 64)
    private String invadersJson;

}
