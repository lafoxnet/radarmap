package net.lafox.alienradar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling

public class AlienRadarApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlienRadarApplication.class, args);
    }
}
