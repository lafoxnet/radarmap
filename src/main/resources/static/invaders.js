function searchMe(val) {
    var formData = {
        "invader": val,
        "radarMap": $("#radarMap").val(),
        "acceptablePointsCount": $("#acceptablePointsCount").val()
    };

    $.postJSON("api/search", formData,
        function (result) { // onSuccess
            console.log(result);
            $('#message').addClass('alert');
            if (result.length > 0) {
                $('#message').html(composeMessage(result));
                $('#message').removeClass('alert-success').addClass('alert-danger');
            } else {
                $('#message').text('Invader Does Not Detected, Yes!');
                $('#message').removeClass('alert-danger').addClass('alert-success');
            }

        },
        function (result) { // onError
            var msg = result.responseJSON;
            var messageText = msg.error + '(' + msg.status + ")\n" + msg.exception + "\nSee server logs for details";
            $('#message').addClass('alert-warning').text(messageText);
            console.log(result);
        }
    );
}


function composeMessage(result) {
    var msg = "Invader detected on point(s):" +
        '<table class="table"><thead><th>#</th><th>X</th><th>Y</th><th>False Points</th></thead>';
    $.each(result, function (index, value) {
        msg += '<tr>' +
            '<td>Invader #' + (index + 1) + '</td>' +
            '<td>' + value.x + '</td>' +
            '<td>' + value.y + '</td>' +
            '<td>' + value.falsePointsCount + '</td>' +
            '</tr>';
    });
    msg += '</table>';
    return msg;
}


$(document).ready(function () {
    $.postJSON = function (url, data, onSuccess, onError) {
        return jQuery.ajax({
            'type': 'POST',
            'url': url,
            'contentType': 'application/json',
            'data': JSON.stringify(data),
            'dataType': 'json',
            'success': onSuccess,
            'error': onError
        });
    };
    $('pre code').each(function (i, block) {
        hljs.highlightBlock(block);
    });
    setInterval(function () {
        $.get("api/getMap?skipLogging=true", function (data) {
            $("#radar").text(data.map);
            $("#date-id").html("ID: " + data.id + " ; " + new Date(data.date) + " GET <a href='/api/getMap' target='_blank'>/api/getMap</a>");
            console.log(data);
        });
    }, 1000);
    $.get("api/getInvaders", function (data) {
        $("#i1").text(data.INVADER_1);
        $("#i2").text(data.INVADER_2);
        $("#i3").text(data.INVADER_3);
        console.log(data);
    });
});